const db = require("../models");
const OlympicWinner = db.olympicWinner;
const Country = db.country;
const Sport = db.sport;
const Op = db.Sequelize.Op;

const getPaginate = (page, size) => {
    const limit = size ? +size : 10;
    const offset = page ? page * limit : 0;

    return {limit, offset};
}

const getPaginateData = (data, page, limit) => {
    const { count:totalItems, rows: olympicWinner } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);

    return { totalItems, olympicWinner, totalPages, currentPage };
}

exports.show = (req, res) => {
    const { athlete, page, size, order_by, order_name } = req.query;
    var condition = athlete ? { athlete: { [Op.like]: `%${athlete}%` } } : null;
    var orderBy = order_by ? [[order_by, order_name]] : null;

    const { limit, offset } = getPaginate(page, size);

    OlympicWinner.findAndCountAll({
        where: condition, limit, offset,
        order: orderBy
        // include: ["countries"]
    }).then(data => {
        const response = getPaginateData(data, page, limit);
        res.send(response);
    }).catch(err => {
        res.status(500).send({
            message:
                err.message || "error"
        });
    });
};