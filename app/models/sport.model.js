module.exports = (sequelize, Sequelize) => {
    const sport = sequelize.define("sports", {
        name: {
            type: Sequelize.STRING
        }
    },{ timestamps: false });

    return sport;
};