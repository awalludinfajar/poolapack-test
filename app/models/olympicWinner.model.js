module.exports = (sequelize, Sequelize) => {
    const olympicWinner = sequelize.define("olympic_winners", {
        athlete: {
            type: Sequelize.STRING
        },
        age: {
            type: Sequelize.BIGINT
        },
        countryId: {
            type: Sequelize.BIGINT
        },
        country_gorup: {
            type: Sequelize.STRING
        },
        year: {
            type: Sequelize.BIGINT
        },
        date: {
            type: Sequelize.DATEONLY
        },
        sportId: {
            type: Sequelize.INTEGER
        },
        gold: {
            type: Sequelize.INTEGER
        },
        silver: {
            type: Sequelize.INTEGER
        },
        bronze: {
            type: Sequelize.INTEGER
        },
        total: {
            type: Sequelize.BIGINT
        },
    },{ timestamps: false });

    return olympicWinner;
};