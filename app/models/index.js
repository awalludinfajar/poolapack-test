const dbConfig = require("../../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: 0,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
},{
    define: {
        timestamps: false 
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.olympicWinner = require("./olympicWinner.model.js")(sequelize, Sequelize);
db.country = require("./country.model.js")(sequelize, Sequelize);
db.sport = require("./sport.model.js")(sequelize, Sequelize);

// db.olympicWinner.hasMany(db.country, { as:"countries", foreignKey:"countryId" });
// db.country.belongsTo(db.olympicWinner, {
//     foreginKey:"countryId"
// });
// db.olympicWinner.hasMany(db.sport, { as:"sports" });
// db.sport.belongsTo(db.olympicWinner, {
//     foreginKey:"sportId",
//     as: "sport"
// });

module.exports = db;