module.exports = (sequelize, Sequelize) => {
    const sport = sequelize.define("countries", {
        name: {
            type: Sequelize.STRING
        }
    },{ timestamps: false });

    return sport;
};