module.exports = app => {
    const winnerOlympic = require("../controllers/olympicWinner.controller");

    var router = require("express").Router();

    router.get("/", winnerOlympic.show);

    app.use("/api", router);
};