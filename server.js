const express = require("express");
const cors = require("cors");
// const db = require("./app/models");

const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended:true }));

const PORT = process.env.PORT || 8081;
app.listen(PORT, () => {
    console.log(`Server is running on Port ${PORT}`);
});

app.use(cors());

require("./app/routes/api.routes.js")(app);

// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });